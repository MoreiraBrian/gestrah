<?php
namespace App\Trash;

use App\DestructionType\ComposterInterface;

class Organic extends Recyclable implements ComposterInterface{

    public function destruction()
    {
        echo "Can go to incineration and to the composter.".PHP_EOL.PHP_EOL;
    }

}