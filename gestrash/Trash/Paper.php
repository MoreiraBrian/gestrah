<?php
namespace App\Trash;

use App\DestructionType\PaperInterface;

class Paper extends Recyclable implements PaperInterface{

    public function destruction()
    {
        echo "Can go to incineration and to the Paper recyclage.".PHP_EOL.PHP_EOL;
    }

}