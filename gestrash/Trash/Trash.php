<?php
namespace App\Trash;



abstract class Trash{
    protected $type;
    protected $quantity;
    protected $CO2Incineration;
    
    public function __construct(string $type,$quantity,$CO2Incineration)
    {
        $this->type = $type;
        $this->quantity = $quantity;
        $this->CO2Incineration = $CO2Incineration;
        
    }

   public function getType(){
        return ucfirst($this->type);
    }
    public function setType($type){
        $this->type = $type;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }
    public function setQuantity($quantity){
        $this->quantity = $quantity;
    }

    public function getCO2Incineration()
    {
        return $this->CO2Incineration;
    }
    public function setCO2Incineration($co2incineration){
        $this->CO2Incineration = $co2incineration;
    }



    
}