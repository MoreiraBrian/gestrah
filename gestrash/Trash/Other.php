<?php
namespace App\Trash;

class Other extends Trash{

    public function __toString():string
    {   
        return "Type: ".ucfirst($this->type).PHP_EOL.
        "Quantity: ".$this->quantity.PHP_EOL.
        "Quantity of CO2 in case of incineration: ".$this->CO2Incineration.PHP_EOL;
    }
        public function destruction()
    {
        echo "Go only to incineration.".PHP_EOL.PHP_EOL;
    }
    
}