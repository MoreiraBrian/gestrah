<?php
namespace App\Trash;

abstract class Recyclable extends trash{
    protected $CO2Recycle;
    public function __construct(string $type,$quatity,$CO2Incineration,$CO2Recycle)
    {
        parent::__construct($type,$quatity,$CO2Incineration);
        $this->CO2Recycle = $CO2Recycle;
    }
    public function getCO2Recycle(){
     return $this->CO2Recycle;
    }
    public function setCO2Recycle($co2recycle){
        $this->CO2Recycle = $co2recycle;
    }

        public function __toString():string
    {   
        return "Type: ".ucfirst($this->type).PHP_EOL.
        "Quantity: ".$this->quantity.PHP_EOL.
        "Quantity of CO2 in case of incineration: ".$this->CO2Incineration.PHP_EOL.
        "Quantity of CO2 when recycle: ".$this->CO2Recycle.PHP_EOL;
    }


}