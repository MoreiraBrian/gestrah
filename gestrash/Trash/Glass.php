<?php

namespace App\Trash;

use App\DestructionType\GlassInterface;

class Glass extends Recyclable implements GlassInterface{

    public function destruction()
    {
        echo "Can go to incineration and to the glass recyclage.".PHP_EOL.PHP_EOL;
    }

}