<?php
namespace App\Trash;

use App\DestructionType\IronInterface;

class Iron extends Recyclable implements IronInterface{

        public function destruction()
    {
        echo "Can go to incineration and to the iron recyclage.".PHP_EOL.PHP_EOL;
    }

}