<?php
use App\Services\Composter;
use App\Services\Incinerator;
use App\Services\RecycleGlass;
use App\Services\RecycleIron;
use App\Services\RecyclePaper;
use App\Services\RecyclePlastic;
use App\Trash\Glass;
use App\Trash\Iron;
use App\Trash\Organic;
use App\Trash\Other;
use App\Trash\Paper;
use App\Trash\PC;
use App\Trash\PEHD;
use App\Trash\PET;
use App\Trash\PVC;

$co2JsonFileContent = file_get_contents("co2.json");
$co2 = json_decode($co2JsonFileContent, true);
$strJsonFileContents = file_get_contents("data.json");
$json = json_decode($strJsonFileContents, true);
$town = array();
$districtNumber = 1;

echo "-------------------------\nDistrict:\n-------------------------\n";

foreach($json['quartiers'] as $quartier){
    $town[] = [new Other("Other",$quartier['autre'],$co2['autre']['incineration']),
         new Organic("organic",$quartier['organique'],$co2['organique']['incineration'],$co2['organique']['compostage']),
         new Paper("paper",$quartier['papier'],$co2['papier']['incineration'],$co2['papier']['recyclage']),
         new Iron("iron",$quartier['metaux'],$co2['metaux']['incineration'],$co2['metaux']['recyclage']),
         new Glass("glass",$quartier['verre'],$co2['verre']['incineration'],$co2['verre']['recyclage']),
         new PC("PC",$quartier['plastiques']['PC'],$co2['plastiques']['PC']['incineration'],$co2['plastiques']['PC']['recyclage']),
         new PEHD("PEHD",$quartier['plastiques']['PEHD'],$co2['plastiques']['PEHD']['incineration'],$co2['plastiques']['PEHD']['recyclage']),
         new PVC("PVC",$quartier['plastiques']['PVC'],$co2['plastiques']['PVC']['incineration'],$co2['plastiques']['PVC']['recyclage']),
         new PET("PET",$quartier['plastiques']['PET'],$co2['plastiques']['PET']['incineration'],$co2['plastiques']['PET']['recyclage'])];

}

foreach($town as $district){
    echo PHP_EOL.PHP_EOL."District n°:".$districtNumber.PHP_EOL;
    foreach($district as $trash){
        echo $trash.PHP_EOL;
        }
    $districtNumber ++;
    }

echo "-------------------------\n-------------------------\n";

echo "\n\n\n-------------------------\nServices:\n-------------------------\n";
$townServices = array();
foreach($json['services'] as $service){
    
    switch($service['type']){
    case "incinerateur":
        $townServices[] = new Incinerator("Incinerator", $service["ligneFour"] , $service["capaciteLigne"]);
        break;
    case "recyclagePlastique":
        $townServices[] = new RecyclePlastic('Recycle plastic industrie', $service['capacite'], $service['plastiques']);
        break;
    case "recyclagePapier":
        $townServices[] = new RecyclePaper("Recycle paper industry",$service['capacite']);
        break;
    case "recyclageVerre":
        $townServices[] = new RecycleGlass("Recycle glass industry", $service['capacite']);
        break;
    case "recyclageMetaux":
        $townServices[] = new RecycleIron("Recycle Iron Industry", $service['capacite']);
        break;
    case "composteur":
        $townServices[] = new Composter("Composter",$service['capacite']);
        break;
    };


}
foreach($townServices as $service){
    echo $service.PHP_EOL;
}

echo "-------------------------\n-------------------------\n";

echo "\n\n\n-------------------------\ntreatments:\n-------------------------\n";
echo "-------------------------\n-------------------------\n";