<?php
namespace App\Services;

abstract class Service{
protected $Type;
protected $Capacity;

public function __construct(string $Type,int $Capacity)
    {
        $this->Type = ucfirst($Type);
        $this->Capacity = $Capacity;
    }

    public function __toString():string
    {
        return "Type: ".$this->Type.PHP_EOL."Capacity: ".$this->Capacity.PHP_EOL.PHP_EOL;
    }

    public function getType(){
        return $this->Type;
    }
    public function setType($type){
        $this->Type = $type;
    }

    public function getCapacity(){
        return $this->Capacity;
    }
    public function setCapacity($capacity){
        $this->Capacity = $capacity;
    }

}