<?php
namespace App\Services;

class Incinerator extends Service{

    protected $Oven;
    protected $OvenCapacity;

    public function __construct (string $Type, $Oven,$OvenCapacity)
    {
        $this->Oven = $Oven;
        $this->OvenCapacity = $OvenCapacity;
        parent::__construct($Type,($Oven*$OvenCapacity));
    }

    public function __toString(): string
    {
        return "Type: ".$this->Type.PHP_EOL.
        "Number of ovens: ".$this->Oven.PHP_EOL.
        "Oven capacity: ".$this->OvenCapacity.PHP_EOL.
        "Total capacity: ".$this->Capacity.PHP_EOL.PHP_EOL;
    }

}