<?php
namespace App\Services;

class RecyclePlastic extends Service{

    protected $Accepted;

    public function __construct(string $Type, $Capacity, array $Accepted = array())
    {
        parent::__construct($Type,$Capacity);
        foreach($Accepted as $Accept) {
        $this->Accepted[] = $Accept;
        }
    }

        public function __toString():string
    {
        return "Type: ".$this->Type.PHP_EOL."Capacity: ".$this->Capacity.PHP_EOL.
        "Accepted plastics: ".implode(", ",$this->Accepted).PHP_EOL.PHP_EOL;
    }

    public function getAccepted(): array
    {
    return $this->Accepted;
    }

}