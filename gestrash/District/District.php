<?php
namespace App\District;

class District{
    protected $nbrPeoples;
    protected $trashs = array();
    
    public function __construct(int $nbrPeoples, array $trashs)
    {
        $this->nbrPeoples = $nbrPeoples;
        foreach($trashs as $trash){
        $this->$trashs[] = $trash;
        }
    }

    public function getNbrPeoples(){
        return $this->nbrPeoples;
    }

}